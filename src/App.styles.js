import styled from 'styled-components';

export const AppWrapper = styled.div`
  height: 100%;
  position: relative;

  .content {
    display: flex;
    height: 100%;
    flex-direction: column;
    align-items: center;

    .no-data {
      display: flex;
      height: 80%;
      justify-content: center;
      align-items: center;
      color: black;
      font-size: 1.5em;
      font-weight: 500;
      text-align: center;
    }
  }
`;
