import { useEffect, useState } from 'react';
import { OPTIONS } from './components';
import { userDataMock } from './mocks/userData';

const URL = 'https://randomuser.me/api/?inc=name,gender,location,email,picture,phone&results=20';

// Create uuid
export const uuid = () => {
  return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
    ((c ^ crypto.getRandomValues(new Uint8Array(1))[0]) & (15 >> c / 4)).toString(16)
  );
}

// Fake promise with some delay (in milliseconds)
export const mockAPIDelay = (data, delay = 700) =>
  new Promise((resolve) => {
    setTimeout(() => {
      resolve(data);
    }, delay);
  });

// Get mocked data
export const getData = async (isMock) => {
  let result = [];

  try {
    if (isMock) {
      result = await mockAPIDelay(userDataMock);
    } else {
      result = await fetch(
        URL,
        { mode: 'cors' }
      )
        .then(
          (response) => response.json()
        );
    }
  } catch (error) {
    throw new Error(`Boom! ${error}`)
  }

  return result;
};

// Get formatted phone US number
export const formatPhoneNumber = (phoneNumberString) => {
  const cleaned = ('' + phoneNumberString).replace(/\D/g, '');
  const match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/);
  if (match) {
    return '(' + match[1] + ') ' + match[2] + '-' + match[3];
  }
  return null;
}

// Hook
export const useDebounce = (value, delay) => {
  // State and setters for debounced value
  const [debouncedValue, setDebouncedValue] = useState(value);
  useEffect(
    () => {
      // Update debounced value after delay
      const handler = setTimeout(() => {
        setDebouncedValue(value);
      }, delay);
      // Cancel the timeout if value changes (also on delay change or unmount)
      // This is how we prevent debounced value from updating if value is changed ...
      // .. within the delay period. Timeout gets cleared and restarted.
      return () => {
        clearTimeout(handler);
      };
    },
    [value, delay] // Only re-call effect if value or delay changes
  );
  return debouncedValue;
}

// Sort by attribute
export const sortByAttribute = (people, attribute) => {
  const sorted = people.sort((a, b) => {
    let attrA;
    let attrB;

    if (attribute === OPTIONS[0].value) { // name
      attrA = a.name.last.toUpperCase();
      attrB = b.name.last.toUpperCase();
    }

    if (attribute === OPTIONS[1].value) { // email
      attrA = a.email.toUpperCase();
      attrB = b.email.toUpperCase();
    }

    if (attribute === OPTIONS[2].value) { // phone
      attrA = a.phone;
      attrB = b.phone;
    }

    if (attribute === OPTIONS[3].value) { // location
      attrA = a.location.country.toUpperCase();
      attrB = b.location.country.toUpperCase();
    }

    if (attrA < attrB) {
      return -1;
    }
    if (attrA > attrB) {
      return 1;
    }
    // names must be equal
    return 0;
  });

  return sorted.map((person, i) => {
    return ({
      ...person,
      order: i
    });
  });
}
