/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from 'react';

import { AppWrapper } from './App.styles';
import { getData, sortByAttribute, useDebounce, uuid } from './utils';
import { Grid, Loading, Modal, Searchbar, Sort, Topbar } from './components';
import { OPTIONS } from './components';

const App = () => {
  const [data, setData] = useState([]);
  const [filteredList, setFilteredList] = useState([]);
  const [query, setQuery] = useState('');
  const [sortBy, setSortBy] = useState(OPTIONS[0].value);
  const [isLoading, setIsLoading] = useState(true);
  const [person, setPerson] = useState({});
  const [isModalOpen, setIsModalOpen] = useState(false);

  const debouncedSearchTerm = useDebounce(query, 800);

  const handleClose = () => setIsModalOpen(false);

  const handleEdit = (id) => {
    const filterPerson = data.filter((per) => per.id === id)[0];
    setPerson(filterPerson);
    setIsModalOpen(true);
  };

  const handleSave = (newPerson) => {
    const newData = data.map((item) => {
      if (item.id === newPerson.id) {
        return newPerson;
      }

      return item;
    });

    const newFilteredList = filteredList.map((item) => {
      if (item.id === newPerson.id) {
        return newPerson;
      }

      return item;
    });

    setData(newData);
    setFilteredList(newFilteredList);
    setIsModalOpen(false);
  };

  useEffect(() => {
    // If we have a search query
    if (debouncedSearchTerm) {
      let result = [];
      // Filter through fields
      result = [
        ...data.filter((person) => person.name.first.toLowerCase().includes(query)),
        ...data.filter((person) => person.name.last.toLowerCase().includes(query)),
        ...data.filter((person) => person.phone.toLowerCase().includes(query)),
        ...data.filter((person) => person.email.toLowerCase().includes(query)),
        ...data.filter((person) => person.location.country.toLowerCase().includes(query))
      ];

      // Normalize result and convert to array
      const uniqueData = Array.from(new Set(result));

      // sort and then save result
      setFilteredList(sortByAttribute(uniqueData, sortBy));
    }
  }, [debouncedSearchTerm]);

  useEffect(() => {
    setData(sortByAttribute(data, sortBy));
  }, [sortBy]);

  useEffect(() => {
    getData() // if true then getData returns mocked data
      .then(({ results }) => {
        const result = results.map((result) => {
          return ({
            ...result,
            id: uuid()
          });
        });

        // sort data
        setData(sortByAttribute(result, sortBy));
      })
      .catch(console.error)
      .finally(() => setIsLoading(false));
  }, []);

  const hasQuery = query.length > 0

  return (
    <AppWrapper className="app">
      {isLoading && (<Loading />)}
      {!isLoading && (
        <div className="content">
          <Topbar>
            <Searchbar value={query} onChange={setQuery} />
            <Sort selected={sortBy} onChange={setSortBy} />
          </Topbar>
          <Grid data={(hasQuery && filteredList) || data} onEdit={handleEdit} />
        </div>
      )}
      {isModalOpen && (
        <Modal person={person} onClose={handleClose} onSave={handleSave} />
      )}
    </AppWrapper>
  );
}

export default App;
