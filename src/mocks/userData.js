export const userDataMock = {
  "results": [{
    "gender": "female",
    "name": {
      "title": "Mrs",
      "first": "Cecilia",
      "last": "Aanstad"
    },
    "location": {
      "street": {
        "number": 7842,
        "name": "Dalsløkka"
      },
      "city": "Flateby",
      "state": "Telemark",
      "country": "Norway",
      "postcode": "4100",
      "coordinates": {
        "latitude": "45.3131",
        "longitude": "13.8508"
      },
      "timezone": {
        "offset": "+2:00",
        "description": "Kaliningrad, South Africa"
      }
    },
    "email": "cecilia.aanstad@example.com",
    "phone": "25586549",
    "picture": {
      "large": "https://randomuser.me/api/portraits/women/25.jpg",
      "medium": "https://randomuser.me/api/portraits/med/women/25.jpg",
      "thumbnail": "https://randomuser.me/api/portraits/thumb/women/25.jpg"
    }
  }, {
    "gender": "male",
    "name": {
      "title": "Monsieur",
      "first": "Filip",
      "last": "Joly"
    },
    "location": {
      "street": {
        "number": 1388,
        "name": "Rue Abel-Hovelacque"
      },
      "city": "St. Margrethen",
      "state": "Jura",
      "country": "Switzerland",
      "postcode": 7012,
      "coordinates": {
        "latitude": "78.2871",
        "longitude": "158.0513"
      },
      "timezone": {
        "offset": "+10:00",
        "description": "Eastern Australia, Guam, Vladivostok"
      }
    },
    "email": "filip.joly@example.com",
    "phone": "078 424 69 09",
    "picture": {
      "large": "https://randomuser.me/api/portraits/men/47.jpg",
      "medium": "https://randomuser.me/api/portraits/med/men/47.jpg",
      "thumbnail": "https://randomuser.me/api/portraits/thumb/men/47.jpg"
    }
  }, {
    "gender": "female",
    "name": {
      "title": "Mrs",
      "first": "Molly",
      "last": "Butler"
    },
    "location": {
      "street": {
        "number": 8203,
        "name": "Church Road"
      },
      "city": "Stirling",
      "state": "East Sussex",
      "country": "United Kingdom",
      "postcode": "RK6A 3GW",
      "coordinates": {
        "latitude": "38.0569",
        "longitude": "-9.1024"
      },
      "timezone": {
        "offset": "+2:00",
        "description": "Kaliningrad, South Africa"
      }
    },
    "email": "molly.butler@example.com",
    "phone": "019467 96998",
    "picture": {
      "large": "https://randomuser.me/api/portraits/women/65.jpg",
      "medium": "https://randomuser.me/api/portraits/med/women/65.jpg",
      "thumbnail": "https://randomuser.me/api/portraits/thumb/women/65.jpg"
    }
  }, {
    "gender": "female",
    "name": {
      "title": "Miss",
      "first": "Vida",
      "last": "Lindanger"
    },
    "location": {
      "street": {
        "number": 8971,
        "name": "Tokerudberget"
      },
      "city": "Svelvik",
      "state": "Buskerud",
      "country": "Norway",
      "postcode": "2630",
      "coordinates": {
        "latitude": "1.2819",
        "longitude": "6.2072"
      },
      "timezone": {
        "offset": "+3:30",
        "description": "Tehran"
      }
    },
    "email": "vida.lindanger@example.com",
    "phone": "72239436",
    "picture": {
      "large": "https://randomuser.me/api/portraits/women/57.jpg",
      "medium": "https://randomuser.me/api/portraits/med/women/57.jpg",
      "thumbnail": "https://randomuser.me/api/portraits/thumb/women/57.jpg"
    }
  }, {
    "gender": "male",
    "name": {
      "title": "Mr",
      "first": "Philip",
      "last": "Andersen"
    },
    "location": {
      "street": {
        "number": 3571,
        "name": "St. Lawrence Ave"
      },
      "city": "Bath",
      "state": "Québec",
      "country": "Canada",
      "postcode": "N8B 8E9",
      "coordinates": {
        "latitude": "-76.9328",
        "longitude": "-9.2491"
      },
      "timezone": {
        "offset": "+8:00",
        "description": "Beijing, Perth, Singapore, Hong Kong"
      }
    },
    "email": "philip.andersen@example.com",
    "phone": "B09 H34-4430",
    "picture": {
      "large": "https://randomuser.me/api/portraits/men/74.jpg",
      "medium": "https://randomuser.me/api/portraits/med/men/74.jpg",
      "thumbnail": "https://randomuser.me/api/portraits/thumb/men/74.jpg"
    }
  }, {
    "gender": "female",
    "name": {
      "title": "Ms",
      "first": "Madelene",
      "last": "Hessen"
    },
    "location": {
      "street": {
        "number": 3043,
        "name": "Lindemans gate"
      },
      "city": "Knappskog",
      "state": "Trøndelag",
      "country": "Norway",
      "postcode": "5509",
      "coordinates": {
        "latitude": "0.9247",
        "longitude": "-15.5529"
      },
      "timezone": {
        "offset": "-9:00",
        "description": "Alaska"
      }
    },
    "email": "madelene.hessen@example.com",
    "phone": "68798228",
    "picture": {
      "large": "https://randomuser.me/api/portraits/women/37.jpg",
      "medium": "https://randomuser.me/api/portraits/med/women/37.jpg",
      "thumbnail": "https://randomuser.me/api/portraits/thumb/women/37.jpg"
    }
  }, {
    "gender": "female",
    "name": {
      "title": "Miss",
      "first": "Lori",
      "last": "Ryan"
    },
    "location": {
      "street": {
        "number": 727,
        "name": "Wycliff Ave"
      },
      "city": "Devonport",
      "state": "Queensland",
      "country": "Australia",
      "postcode": 7504,
      "coordinates": {
        "latitude": "12.3930",
        "longitude": "61.3430"
      },
      "timezone": {
        "offset": "+2:00",
        "description": "Kaliningrad, South Africa"
      }
    },
    "email": "lori.ryan@example.com",
    "phone": "08-1628-3744",
    "picture": {
      "large": "https://randomuser.me/api/portraits/women/2.jpg",
      "medium": "https://randomuser.me/api/portraits/med/women/2.jpg",
      "thumbnail": "https://randomuser.me/api/portraits/thumb/women/2.jpg"
    }
  }, {
    "gender": "female",
    "name": {
      "title": "Mademoiselle",
      "first": "Jane",
      "last": "Meyer"
    },
    "location": {
      "street": {
        "number": 638,
        "name": "Cours Charlemagne"
      },
      "city": "Turgi",
      "state": "Zug",
      "country": "Switzerland",
      "postcode": 1233,
      "coordinates": {
        "latitude": "-82.0019",
        "longitude": "119.4801"
      },
      "timezone": {
        "offset": "+8:00",
        "description": "Beijing, Perth, Singapore, Hong Kong"
      }
    },
    "email": "jane.meyer@example.com",
    "phone": "077 512 85 64",
    "picture": {
      "large": "https://randomuser.me/api/portraits/women/96.jpg",
      "medium": "https://randomuser.me/api/portraits/med/women/96.jpg",
      "thumbnail": "https://randomuser.me/api/portraits/thumb/women/96.jpg"
    }
  }, {
    "gender": "male",
    "name": {
      "title": "Monsieur",
      "first": "António",
      "last": "Lefevre"
    },
    "location": {
      "street": {
        "number": 2739,
        "name": "Rue de la Barre"
      },
      "city": "Stansstad",
      "state": "Schwyz",
      "country": "Switzerland",
      "postcode": 5357,
      "coordinates": {
        "latitude": "27.9060",
        "longitude": "-39.3778"
      },
      "timezone": {
        "offset": "-5:00",
        "description": "Eastern Time (US & Canada), Bogota, Lima"
      }
    },
    "email": "antonio.lefevre@example.com",
    "phone": "077 315 51 49",
    "picture": {
      "large": "https://randomuser.me/api/portraits/men/69.jpg",
      "medium": "https://randomuser.me/api/portraits/med/men/69.jpg",
      "thumbnail": "https://randomuser.me/api/portraits/thumb/men/69.jpg"
    }
  }, {
    "gender": "male",
    "name": {
      "title": "Mr",
      "first": "Sean",
      "last": "Vargas"
    },
    "location": {
      "street": {
        "number": 4450,
        "name": "Green Rd"
      },
      "city": "Irving",
      "state": "Delaware",
      "country": "United States",
      "postcode": 56995,
      "coordinates": {
        "latitude": "-8.9239",
        "longitude": "13.0985"
      },
      "timezone": {
        "offset": "+5:00",
        "description": "Ekaterinburg, Islamabad, Karachi, Tashkent"
      }
    },
    "email": "sean.vargas@example.com",
    "phone": "(985) 392-1158",
    "picture": {
      "large": "https://randomuser.me/api/portraits/men/87.jpg",
      "medium": "https://randomuser.me/api/portraits/med/men/87.jpg",
      "thumbnail": "https://randomuser.me/api/portraits/thumb/men/87.jpg"
    }
  }, {
    "gender": "male",
    "name": {
      "title": "Mr",
      "first": "Simon",
      "last": "Petersen"
    },
    "location": {
      "street": {
        "number": 6745,
        "name": "Blommehaven"
      },
      "city": "Lundby",
      "state": "Midtjylland",
      "country": "Denmark",
      "postcode": 88697,
      "coordinates": {
        "latitude": "20.1658",
        "longitude": "-154.0883"
      },
      "timezone": {
        "offset": "-1:00",
        "description": "Azores, Cape Verde Islands"
      }
    },
    "email": "simon.petersen@example.com",
    "phone": "17265401",
    "picture": {
      "large": "https://randomuser.me/api/portraits/men/30.jpg",
      "medium": "https://randomuser.me/api/portraits/med/men/30.jpg",
      "thumbnail": "https://randomuser.me/api/portraits/thumb/men/30.jpg"
    }
  }, {
    "gender": "female",
    "name": {
      "title": "Miss",
      "first": "Anna",
      "last": "Daniels"
    },
    "location": {
      "street": {
        "number": 768,
        "name": "Strand Road"
      },
      "city": "Athy",
      "state": "Kilkenny",
      "country": "Ireland",
      "postcode": 91787,
      "coordinates": {
        "latitude": "45.8181",
        "longitude": "72.1770"
      },
      "timezone": {
        "offset": "+5:45",
        "description": "Kathmandu"
      }
    },
    "email": "anna.daniels@example.com",
    "phone": "051-928-3190",
    "picture": {
      "large": "https://randomuser.me/api/portraits/women/89.jpg",
      "medium": "https://randomuser.me/api/portraits/med/women/89.jpg",
      "thumbnail": "https://randomuser.me/api/portraits/thumb/women/89.jpg"
    }
  }, {
    "gender": "female",
    "name": {
      "title": "Mrs",
      "first": "Vickie",
      "last": "Burton"
    },
    "location": {
      "street": {
        "number": 2448,
        "name": "Royal Ln"
      },
      "city": "Melbourne",
      "state": "Tasmania",
      "country": "Australia",
      "postcode": 7940,
      "coordinates": {
        "latitude": "46.0052",
        "longitude": "-39.3462"
      },
      "timezone": {
        "offset": "-8:00",
        "description": "Pacific Time (US & Canada)"
      }
    },
    "email": "vickie.burton@example.com",
    "phone": "04-5804-5402",
    "picture": {
      "large": "https://randomuser.me/api/portraits/women/78.jpg",
      "medium": "https://randomuser.me/api/portraits/med/women/78.jpg",
      "thumbnail": "https://randomuser.me/api/portraits/thumb/women/78.jpg"
    }
  }, {
    "gender": "male",
    "name": {
      "title": "Mr",
      "first": "Randolfo",
      "last": "da Luz"
    },
    "location": {
      "street": {
        "number": 6418,
        "name": "Rua Das Flores "
      },
      "city": "Tatuí",
      "state": "Bahia",
      "country": "Brazil",
      "postcode": 16045,
      "coordinates": {
        "latitude": "8.2825",
        "longitude": "-175.1974"
      },
      "timezone": {
        "offset": "-6:00",
        "description": "Central Time (US & Canada), Mexico City"
      }
    },
    "email": "randolfo.daluz@example.com",
    "phone": "(06) 3351-7401",
    "picture": {
      "large": "https://randomuser.me/api/portraits/men/33.jpg",
      "medium": "https://randomuser.me/api/portraits/med/men/33.jpg",
      "thumbnail": "https://randomuser.me/api/portraits/thumb/men/33.jpg"
    }
  }, {
    "gender": "female",
    "name": {
      "title": "Miss",
      "first": "Florence",
      "last": "Henderson"
    },
    "location": {
      "street": {
        "number": 8510,
        "name": "Queens Road"
      },
      "city": "Bangor",
      "state": "Devon",
      "country": "United Kingdom",
      "postcode": "N9P 2RN",
      "coordinates": {
        "latitude": "-7.5946",
        "longitude": "5.7716"
      },
      "timezone": {
        "offset": "-7:00",
        "description": "Mountain Time (US & Canada)"
      }
    },
    "email": "florence.henderson@example.com",
    "phone": "015242 42313",
    "picture": {
      "large": "https://randomuser.me/api/portraits/women/50.jpg",
      "medium": "https://randomuser.me/api/portraits/med/women/50.jpg",
      "thumbnail": "https://randomuser.me/api/portraits/thumb/women/50.jpg"
    }
  }, {
    "gender": "female",
    "name": {
      "title": "Madame",
      "first": "Flurina",
      "last": "Andre"
    },
    "location": {
      "street": {
        "number": 2270,
        "name": "Rue Abel-Hovelacque"
      },
      "city": "Hüttwilen",
      "state": "Bern",
      "country": "Switzerland",
      "postcode": 2446,
      "coordinates": {
        "latitude": "-85.3267",
        "longitude": "-28.0881"
      },
      "timezone": {
        "offset": "-11:00",
        "description": "Midway Island, Samoa"
      }
    },
    "email": "flurina.andre@example.com",
    "phone": "078 941 01 57",
    "picture": {
      "large": "https://randomuser.me/api/portraits/women/52.jpg",
      "medium": "https://randomuser.me/api/portraits/med/women/52.jpg",
      "thumbnail": "https://randomuser.me/api/portraits/thumb/women/52.jpg"
    }
  }, {
    "gender": "female",
    "name": {
      "title": "Ms",
      "first": "Nicole",
      "last": "Bennett"
    },
    "location": {
      "street": {
        "number": 4292,
        "name": "The Grove"
      },
      "city": "Clonmel",
      "state": "Wexford",
      "country": "Ireland",
      "postcode": 68654,
      "coordinates": {
        "latitude": "23.0030",
        "longitude": "119.8839"
      },
      "timezone": {
        "offset": "0:00",
        "description": "Western Europe Time, London, Lisbon, Casablanca"
      }
    },
    "email": "nicole.bennett@example.com",
    "phone": "011-439-3045",
    "picture": {
      "large": "https://randomuser.me/api/portraits/women/62.jpg",
      "medium": "https://randomuser.me/api/portraits/med/women/62.jpg",
      "thumbnail": "https://randomuser.me/api/portraits/thumb/women/62.jpg"
    }
  }, {
    "gender": "male",
    "name": {
      "title": "Mr",
      "first": "Oliver",
      "last": "Lavigne"
    },
    "location": {
      "street": {
        "number": 1010,
        "name": "Pine Rd"
      },
      "city": "Brockton",
      "state": "Manitoba",
      "country": "Canada",
      "postcode": "J4R 9R0",
      "coordinates": {
        "latitude": "-42.5947",
        "longitude": "-90.8014"
      },
      "timezone": {
        "offset": "+9:30",
        "description": "Adelaide, Darwin"
      }
    },
    "email": "oliver.lavigne@example.com",
    "phone": "A39 M11-8199",
    "picture": {
      "large": "https://randomuser.me/api/portraits/men/54.jpg",
      "medium": "https://randomuser.me/api/portraits/med/men/54.jpg",
      "thumbnail": "https://randomuser.me/api/portraits/thumb/men/54.jpg"
    }
  }, {
    "gender": "male",
    "name": {
      "title": "Mr",
      "first": "Tadija",
      "last": "Stojanović"
    },
    "location": {
      "street": {
        "number": 3753,
        "name": "Đurakovačka"
      },
      "city": "Merošina",
      "state": "Podunavlje",
      "country": "Serbia",
      "postcode": 53611,
      "coordinates": {
        "latitude": "-73.5817",
        "longitude": "-52.0945"
      },
      "timezone": {
        "offset": "+4:00",
        "description": "Abu Dhabi, Muscat, Baku, Tbilisi"
      }
    },
    "email": "tadija.stojanovic@example.com",
    "phone": "022-6926-192",
    "picture": {
      "large": "https://randomuser.me/api/portraits/men/97.jpg",
      "medium": "https://randomuser.me/api/portraits/med/men/97.jpg",
      "thumbnail": "https://randomuser.me/api/portraits/thumb/men/97.jpg"
    }
  }, {
    "gender": "female",
    "name": {
      "title": "Miss",
      "first": "آوا",
      "last": "یاسمی"
    },
    "location": {
      "street": {
        "number": 787,
        "name": "شهید شهرام امیری"
      },
      "city": "بوشهر",
      "state": "زنجان",
      "country": "Iran",
      "postcode": 36060,
      "coordinates": {
        "latitude": "-20.8467",
        "longitude": "134.7917"
      },
      "timezone": {
        "offset": "+10:00",
        "description": "Eastern Australia, Guam, Vladivostok"
      }
    },
    "email": "aw.ysmy@example.com",
    "phone": "046-38870806",
    "picture": {
      "large": "https://randomuser.me/api/portraits/women/55.jpg",
      "medium": "https://randomuser.me/api/portraits/med/women/55.jpg",
      "thumbnail": "https://randomuser.me/api/portraits/thumb/women/55.jpg"
    }
  }],
  "info": {
    "seed": "730bbc31fe35aa8b",
    "results": 20,
    "page": 1,
    "version": "1.4"
  }
};
