import { render } from '@testing-library/react';
import Searchbar from './Searchbar';

describe('<Searchbar />', () => {
  it('should render a default component', () => {
    const valueMock = '';
    const onChangeMock = jest.mock;
    const props = {
      value: valueMock,
      onChange: onChangeMock
    };

    const { container } = render(<Searchbar {...props} />);

    expect(container).toMatchSnapshot();
  });
});
