import styled from 'styled-components';

const SearchbarWrapper = styled.div`
  display: inline-flex;
  position: relative;
  width: 300px;

  input {
    border: 1px solid #dcdcdc;
    border-radius: 30px;
    padding: 10px 20px;
    font-size: 1.2em;
    width: 100%;
    outline: none;

    &:hover {
      box-shadow: 1px 1px 8px 1px #dcdcdc;
    }

    &:focus-within {
      box-shadow: 1px 1px 8px 1px #dcdcdc;
      outline:none;
    }
  }

  span {
    color: #666;
    position: absolute;
    top: 12px;
    right: 20px;
    cursor: pointer;
    user-select: none;
  }
`;

const Searchbar = ({ value, onChange, ...rest }) => {
  const handleOnChange = (e) => onChange(e.target.value);
  const handleOnClear = () => onChange('');

  return (
    <SearchbarWrapper className="search" {...rest}>
      {value.length > 0 && (<span onClick={handleOnClear}>x</span>)}
      <input value={value} onChange={handleOnChange} placeholder="Search user" />
    </SearchbarWrapper>
  );
};

export default Searchbar;
