import styled from 'styled-components';

export const ProfileWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-evenly;
  position: relative;
  width: 250px;
  height: 300px;
  border-radius: 7px;
  background: linear-gradient(to bottom, ${({ gender }) =>
    gender === 'male'
      ? '#416dff 0%,#416dff 35%'
      : '#ce41ff 0%,#ce41ff 35%'},
    #fff 35%,#fff 100%);
  box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;
  border: 3px solid #fff;

  .user-edit {
    font-size: 25px;
    opacity: 0.7;
    cursor: pointer;
    position: absolute;
    top: 15px;
    left: 15px;

    &:hover {
      color: #fffbc8;
      opacity: 1;
    }
  }

  .name {
    font-size: 20px;
    font-weight: 600;
  }

  .picture {
    position: relative;
    overflow: hidden;
    border-radius: 50%;
    border: 5px solid #fff;
    width: 120px;
    height: 120px;
    box-shadow: rgba(0, 0, 0, 0.16) 0px 3px 6px, rgba(0, 0, 0, 0.23) 0px 3px 6px;

    img {
      display: inline;
      margin: 0 auto;
      height: 100%;
      width: auto;
    }
  }

  .email,
  .phone,
  .location {
    color: black;
    text-align: center;
  }

  .name,
  .user-edit,
  .picture,
  .email,
  .phone,
  .location {
    background-color: transparent;
  }
`;
