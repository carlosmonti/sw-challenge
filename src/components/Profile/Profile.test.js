import { render } from '@testing-library/react';

import { userDataMock } from '../../mocks/userData';
import Profile from './Profile';

describe('<Profile />', () => {
  it('should render a default component', () => {
    const personMock = {
      ...userDataMock.results[0]
    };
    const onEditMock = jest.mock;

    const props = {
      person: personMock,
      onEdit: onEditMock,
    };

    const { container } = render(<Profile {...props} />);

    expect(container).toMatchSnapshot();
  });
});
