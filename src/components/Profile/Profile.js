import { FaUserEdit } from 'react-icons/fa';
import { formatPhoneNumber } from '../../utils';
import { ProfileWrapper } from './Profile.styles';

const Profile = ({ person, onEdit, ...rest }) => {
  const { id, name, gender, email, location, phone, picture } = person;
  const fullname = `${name.first} ${name.last}`;

  const onClickHandler = () => {
    return onEdit(id);
  }

  return (
    <ProfileWrapper key={name.last} gender={gender} className="profile" {...rest}>
      <div className="user-edit">
        <FaUserEdit onClick={onClickHandler} />
      </div>
      <div className="name">{`${name.first} ${name.last}`}</div>
      <div className="picture">
        <img src={picture.large} alt={fullname} height="125px" />
      </div>
      <div className="email">{email}</div>
      <div className="phone">{formatPhoneNumber(phone) || phone}</div>
      <div className="location">{`${location.city}, ${location.state}, ${location.country}`}</div>
    </ProfileWrapper>
  );
};

export default Profile;
