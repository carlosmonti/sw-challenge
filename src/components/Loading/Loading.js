import styled from 'styled-components';

const LoadingWrapper = styled.div`
  color: #000;
  font-size: 30px;
  font-weight: 600;
  position: absolute;
  top: calc(50% - 10%);
  left: 0;
  right: 0;
  margin-left: auto;
  margin-right: auto;
  width: 140px;
`;

const Loading = () => {
  return (
    <LoadingWrapper className="loading">Loading...</LoadingWrapper>
  );
};

export default Loading;
