import styled from 'styled-components';

const TopbarWrapper = styled.div`
  width: 100%;
  margin-top: 15px;
  display: flex;
  align-items: center;
  flex-direction: row;
  justify-content: space-around;
`;

const Topbar = ({ children, ...rest }) => {
  return (
    <TopbarWrapper className="topbar" {...rest}>
      {children}
    </TopbarWrapper>
  );
};

export default Topbar;
