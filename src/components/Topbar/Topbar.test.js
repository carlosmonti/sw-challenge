import { render } from '@testing-library/react';
import Topbar from './Topbar';

describe('<Topbar />', () => {
  it('should render a default component', () => {
    const { container } = render(<Topbar>Some stuff</Topbar>);

    expect(container).toMatchSnapshot();
  });
});
