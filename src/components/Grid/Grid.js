import styled from 'styled-components';

import Profile from '../Profile';

const GridWrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-template-rows: repeat(2, 1fr);
  grid-column-gap: 30px;
  grid-row-gap: 30px;
  padding: 20px;
  background-color: transparent;
`;

const Grid = ({ data, onEdit, ...rest }) => {
  if (!data.length) return (
    <div className="no-data">Sorry, no data</div>
  );

  return (
    <GridWrapper className="grid" {...rest}>
      {data.map((profile) => (
        <Profile person={profile} onEdit={onEdit} key={profile.id} />
      ))}
    </GridWrapper>
  );
};

export default Grid;
