import { render } from '@testing-library/react';

import { userDataMock } from '../../mocks/userData';
import Grid from './Grid';

describe('<Grid />', () => {
  it('should render a default grid without data', () => {
    const onEditMock = jest.mock();
    const dataMock = [];

    const props = {
      onEdit: onEditMock,
      data: dataMock,
    };

    const { container } = render(<Grid {...props} />);

    expect(container).toMatchSnapshot();
  });

  it('should render a default grid with data', () => {
    const onEditMock = jest.mock();
    const { results: dataMock } = userDataMock;

    const props = {
      onEdit: onEditMock,
      data: dataMock.map((item, i) => ({
        ...item,
        id: i
      })),
    };

    const { container } = render(<Grid {...props} />);

    expect(container).toMatchSnapshot();
  });
});
