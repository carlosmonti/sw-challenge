import styled from 'styled-components';

const SortWrapper = styled.div`
  font-size: 1em;
`;

export const OPTIONS = [
  { label: 'Name', value: 'name' },
  { label: 'Email', value: 'email' },
  { label: 'Phone', value: 'phone' },
  { label: 'Location', value: 'location' }
];

const Sort = ({ selected, onChange, ...rest }) => {
  const handleOnChange = (e) => onChange(e.target.value);

  return (
    <SortWrapper className="sort" {...rest}>
      <select name="sort" id="sort" onChange={handleOnChange} value={selected}>
        {OPTIONS.map(({ label, value }) => {
          return (
            <option key={value} value={value}>
              {label}
            </option>
          );
        })}
      </select>
    </SortWrapper>
  );
};

export default Sort;
