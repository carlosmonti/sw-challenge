import { render } from '@testing-library/react';
import Sort, { OPTIONS } from './Sort';

describe('<Sort />', () => {
  it('should render a default component', () => {
    const selectedMock = OPTIONS[0].value;
    const onChangeMock = jest.mock;
    const props = {
      selected: selectedMock,
      onChange: onChangeMock
    };

    const { container } = render(<Sort {...props} />);

    expect(container).toMatchSnapshot();
  });
});
