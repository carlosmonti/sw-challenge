import { render } from '@testing-library/react';

import { userDataMock } from '../../mocks/userData';
import Modal from './Modal';

describe('<Modal />', () => {
  it('should render a default component', () => {
    const personMock = {
      ...userDataMock.results[0]
    };
    const onCloseMock = jest.mock;
    const onSaveMock = jest.mock;

    const props = {
      person: personMock,
      onClose: onCloseMock,
      onSave: onSaveMock
    };

    const { container } = render(<Modal {...props} />);

    expect(container).toMatchSnapshot();
  });
});
