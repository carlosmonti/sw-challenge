import styled from 'styled-components';

export const ModalWrapper = styled.div`
  width: 300px;
  position: absolute;
  display: flex;
  flex-direction: column;
  background: white;
  padding: 25px;
  border-radius: 7px;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  box-shadow: rgba(0, 0, 0, 0.56) 0px 22px 70px 4px;

  div {
    color: black;
  }

  label {
    color: #666;
    font-size: 0.9em;
    margin: 5px 0;
  }

  header {
    color: black;
    font-size: 1.5em;
    font-weight: bold;
    margin-bottom: 10px;
  }

  footer {
    margin-top: 15px;
    display: flex;
    justify-content: space-between;

    button {
      background-color: #f5f5f5;
      border: none;
      color: #707070;
      font-size: 15px;
      padding: 10px 20px;
      margin: 5px;
      border-radius: 4px;
      outline: none;

      &:hover{
        border: 1px solid #c8c8c8;
        padding: 9px 19px;
        color:#808080;
      }

      &:focus{
        border:1px solid #4885ed;
        padding: 9px 19px;
      }
    }
  }
`;
