import { useState } from 'react';

import { ModalWrapper } from './Modal.styles';

const Modal = ({ person, onClose, onSave, ...rest }) => {
  const [newPerson, setNewPerson] = useState(person);

  const handleOnSave = () => {
    onSave(newPerson);
  }

  const handleOnChange = (event) => {
    const { name, value } = event.target;
    setNewPerson({
      ...newPerson,
      [name]: value
    });
  };

  return (
    <ModalWrapper className="modal" {...rest}>
      <header>Edit</header>
      <label htmlFor="email">Email</label>
      <input type="text" name="email" onChange={handleOnChange} defaultValue={person.email} />
      <label htmlFor="phone">Phone</label>
      <input type="text" name="phone" onChange={handleOnChange} defaultValue={person.phone} />
      <label htmlFor="location.city">City</label>
      <input type="text" name="location.city" onChange={handleOnChange} defaultValue={person.location.city} />
      <label htmlFor="location.country">Country</label>
      <input type="text" name="location.country" onChange={handleOnChange} defaultValue={person.location.country} />
      <label htmlFor="location.postcode">Postal code</label>
      <input type="text" name="location.postcode" onChange={handleOnChange} defaultValue={person.location.postcode} />
      <label htmlFor="location.state">State</label>
      <input type="text" name="location.state" onChange={handleOnChange} defaultValue={person.location.state} />
      <label htmlFor="location.street.number">Street number</label>
      <input type="text" name="location.street.number" onChange={handleOnChange} defaultValue={person.location.street.number} />
      <label htmlFor="location.street.name">Street name</label>
      <input type="text" name="location.street.name" onChange={handleOnChange} defaultValue={person.location.street.name} />
      <footer>
        <button className="cancel" type="button" onClick={onClose}>Cancel</button>
        <button className="save" type="button" onClick={handleOnSave}>Save</button>
      </footer>
    </ModalWrapper>
  );
};

export default Modal;
