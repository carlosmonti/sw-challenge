# Technical Challenge

## Instructions

1. clone the repo `https://gitlab.com/carlosmonti/sw-challenge.git`
2. build it `npm i`
3. start it `npm start`

4. check it out locally `http://localhost:3000/`

[Preview deployed version](https://carlosmonti.gitlab.io/sw-challenge/)

[Stackblitz Version](https://stackblitz.com/edit/react-hdw2a1)

---

Hi Dear Candidate,
We are Southteams a US based company with a lot of opportunities to grow your career internationally! All our positions are intended to be long term, you'll work as part of the core team of a product company, 100% remote and paid in US dollars. Our only MUST is that you need to be able to communicate in english.

At Southteams we believe we all are much more than a resume, we believe we are defined by the quality of the work we produce. So let's see your code!

Would you please complete this simple test? It shouldn't take you long, we'll be waiting for your results to move forward.

We can't wait to know more about you!

---

## CODE TEST

Using [https://stackblitz.com/](https://stackblitz.com/) or [https://codesandbox.io/](https://codesandbox.io/) create a React application that will load a list of users from this api [https://randomuser.me/](https://randomuser.me/)

Store the result of the request and display the information in a grid of cards styled as seen below.

![image](https://gitlab.com/carlosmonti/sw-challenge/uploads/b4f43e23f804c5493f08e4ee07b25835/image.png)

### Include following functionality in the list

* A search bar that will filter the list based on the user input.
* Ability to sort the list by one of the fields.
* Ability to edit a list item and have the card update with the new information (only in local state not through the api)

You can use JS libraries such as lodash or underscore and a CSS or SCSS normalizer to make it easier to accomplish this task but we are most interested in your own code so please refrain from bringing in other libraries. Try to Include comments in the app letting us know your thought process.

Save the project on stackblitz and share the code link with us as soon as you’re ready.

Feel free to reach out to me with any questions.
